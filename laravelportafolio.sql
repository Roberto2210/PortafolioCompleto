-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-11-2017 a las 01:34:53
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laravelportafolio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coments`
--

CREATE TABLE `coments` (
  `id_coment` int(10) UNSIGNED NOT NULL,
  `asunto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FK_idInfoPublic` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `files`
--

CREATE TABLE `files` (
  `id_file` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateStart` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FKid_person` int(10) UNSIGNED NOT NULL,
  `FKcodeFile` int(10) UNSIGNED NOT NULL,
  `FKstate` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `files`
--

INSERT INTO `files` (`id_file`, `name`, `dateStart`, `description`, `path`, `FKid_person`, `FKcodeFile`, `FKstate`) VALUES
(1, 'el mundo', '2017-11-21', '<p>el mskmd sdkjsk&nbsp;<img src="js/editorText/tinymce/plugins/emoticons/img/smiley-laughing.gif" alt="laughing" /></p>', '26imagenes-png.png', 1, 2, 2),
(2, 'Windows 10', '2017-11-15', '<p>dcfvgbhnjmk,l hjdjkshkjf dfd<img src="js/editorText/tinymce/plugins/emoticons/img/smiley-money-mouth.gif" alt="money-mouth" />&nbsp; dhsjkfhkdshfk<span style="color: #008080;"> jdkjshfjkdshfdhsfk&nbsp;<em>nejfhdskfjdskfhdsjkfhdkjsf</em></span></p>\r\n<p><span style="color: #008080;"><em>dsfjdsklfjldsjf</em></span></p>', '19download.jpg', 1, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `info_publics`
--

CREATE TABLE `info_publics` (
  `id_info` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `FKid_archivo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(60, '2017_10_03_100000_create_password_resets_table', 1),
(61, '2017_10_03_121149_create_permissions_table', 1),
(62, '2017_10_03_121150_create_states_table', 1),
(63, '2017_10_03_121152_create_people_table', 1),
(64, '2017_10_03_121239_create_type_files_table', 1),
(65, '2017_10_03_121434_create_files_table', 1),
(66, '2017_10_03_121503_create_info_publics_table', 1),
(67, '2017_10_03_121614_create_coments_table', 1),
(68, '2017_10_28_144257_create_services_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `nameUser` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `people`
--

CREATE TABLE `people` (
  `id_person` int(10) UNSIGNED NOT NULL,
  `document_Ident` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FKpermission` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `people`
--

INSERT INTO `people` (`id_person`, `document_Ident`, `name`, `last_Name`, `email`, `password`, `path`, `FKpermission`) VALUES
(1, 99102201406, 'Jose Roberto', 'Saavedra Carvajal', 'jrsaavedra60@misena.edu.co', '22102010', '54luffy.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id_permission` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id_permission`, `name`) VALUES
(1, 'Activo'),
(2, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE `services` (
  `id_service` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `valor` double NOT NULL,
  `FKperson` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `services`
--

INSERT INTO `services` (`id_service`, `name`, `description`, `valor`, `FKperson`) VALUES
(5, 'servicio', '<p style="padding-left: 30px;"><em>vgbhnjmk,<strong>dcfvgbhnjm,</strong></em></p>', 567890, 1),
(6, 'servicio de vwntas robert', '<p style="text-align: center;">Hola este&nbsp;<span style="color: #ff6600;">mi servicio&nbsp;<strong>espero les sea de su agrado</strong></span></p>\n<p style="text-align: center;"><span style="color: #0000ff;"><strong>Att: <span style="color: #800000;">Roberto Saavedra&nbsp;<img src="js/editorText/tinymce/plugins/emoticons/img/smiley-cool.gif" alt="cool" /></span></strong></span></p>\n<p style="text-align: center;"><span style="color: #0000ff;"><strong><span style="color: #800000;"><a title="level" href="https://stackoverflow.com/questions/18822405/tinymce-default-tollbar-specification">https://stackoverflow.com/questions/18822405/tinymce-default-tollbar-specification</a></span></strong></span></p>\n<p style="text-align: center;">&nbsp;</p>', 789087, 1),
(7, 'fvgbhjn', '<p>fcvgbhjnkm</p>', 56789, 1),
(8, 'what', '<p>jdnfjkddf djfkljdkfl dmfkljdlf&nbsp;<img src="js/editorText/tinymce/plugins/emoticons/img/smiley-cool.gif" alt="cool" />&nbsp;dfnj djfkjdkjf dkjfkjdf&nbsp;<img src="js/editorText/tinymce/plugins/emoticons/img/smiley-laughing.gif" alt="laughing" />&nbsp;dkhjdkhfjk kdllfjdk<img src="js/editorText/tinymce/plugins/emoticons/img/smiley-sealed.gif" alt="sealed" />&nbsp; &nbsp;eelm nndjjd</p>\r\n<p><em>dkjnfdjsnfjdsf dnfsnfjkdnsfnds dsnfjdnskf jdkfjkdsl<span style="background-color: #ff6600;"> njsdhjksdksjdk<span style="color: #333300;"> skjdjkshj<span style="background-color: #ff6600;">&nbsp;&nbsp;</span></span></span></em></p>', 676767, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `states`
--

CREATE TABLE `states` (
  `id_state` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `states`
--

INSERT INTO `states` (`id_state`, `name`) VALUES
(2, 'Inactivo'),
(3, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_files`
--

CREATE TABLE `type_files` (
  `code` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `type_files`
--

INSERT INTO `type_files` (`code`, `name`) VALUES
(1, 'Mis proyectos'),
(2, 'Trabajos'),
(3, 'APPS'),
(4, 'Programación');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `coments`
--
ALTER TABLE `coments`
  ADD PRIMARY KEY (`id_coment`),
  ADD UNIQUE KEY `coments_email_unique` (`email`),
  ADD KEY `coments_fk_idinfopublic_foreign` (`FK_idInfoPublic`);

--
-- Indices de la tabla `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id_file`),
  ADD UNIQUE KEY `files_path_unique` (`path`),
  ADD KEY `files_fkid_person_foreign` (`FKid_person`),
  ADD KEY `files_fkcodefile_foreign` (`FKcodeFile`),
  ADD KEY `files_fkstate_foreign` (`FKstate`);

--
-- Indices de la tabla `info_publics`
--
ALTER TABLE `info_publics`
  ADD PRIMARY KEY (`id_info`),
  ADD KEY `info_publics_fkid_archivo_foreign` (`FKid_archivo`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_nameuser_index` (`nameUser`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id_person`),
  ADD UNIQUE KEY `people_email_unique` (`email`),
  ADD KEY `people_fkpermission_foreign` (`FKpermission`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id_permission`);

--
-- Indices de la tabla `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id_service`),
  ADD KEY `services_fkperson_foreign` (`FKperson`);

--
-- Indices de la tabla `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id_state`);

--
-- Indices de la tabla `type_files`
--
ALTER TABLE `type_files`
  ADD PRIMARY KEY (`code`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `coments`
--
ALTER TABLE `coments`
  MODIFY `id_coment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `files`
--
ALTER TABLE `files`
  MODIFY `id_file` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `info_publics`
--
ALTER TABLE `info_publics`
  MODIFY `id_info` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT de la tabla `people`
--
ALTER TABLE `people`
  MODIFY `id_person` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id_permission` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `services`
--
ALTER TABLE `services`
  MODIFY `id_service` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `states`
--
ALTER TABLE `states`
  MODIFY `id_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `type_files`
--
ALTER TABLE `type_files`
  MODIFY `code` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `coments`
--
ALTER TABLE `coments`
  ADD CONSTRAINT `coments_fk_idinfopublic_foreign` FOREIGN KEY (`FK_idInfoPublic`) REFERENCES `info_publics` (`id_info`);

--
-- Filtros para la tabla `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_fkcodefile_foreign` FOREIGN KEY (`FKcodeFile`) REFERENCES `type_files` (`code`),
  ADD CONSTRAINT `files_fkid_person_foreign` FOREIGN KEY (`FKid_person`) REFERENCES `people` (`id_person`),
  ADD CONSTRAINT `files_fkstate_foreign` FOREIGN KEY (`FKstate`) REFERENCES `states` (`id_state`);

--
-- Filtros para la tabla `info_publics`
--
ALTER TABLE `info_publics`
  ADD CONSTRAINT `info_publics_fkid_archivo_foreign` FOREIGN KEY (`FKid_archivo`) REFERENCES `files` (`id_file`);

--
-- Filtros para la tabla `people`
--
ALTER TABLE `people`
  ADD CONSTRAINT `people_fkpermission_foreign` FOREIGN KEY (`FKpermission`) REFERENCES `permissions` (`id_permission`);

--
-- Filtros para la tabla `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_fkperson_foreign` FOREIGN KEY (`FKperson`) REFERENCES `people` (`id_person`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
